package pl.com.milisoft.shophunter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.com.milisoft.shophunter.model.MonitoredItem;
import pl.com.milisoft.shophunter.model.User;
import pl.com.milisoft.shophunter.repository.MonitoredItemRepository;
import pl.com.milisoft.shophunter.repository.UserRepository;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/monitor")
public class MonitoringController {

    @Autowired
    private MonitoredItemRepository monitoredItemRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public String getAllItems(Model model, Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        List<MonitoredItem> items = monitoredItemRepository.findByUserEmail(user.getEmail());
        model.addAttribute("items", items);
        return "monitor";
    }

    @PostMapping
    public String addItem(@ModelAttribute MonitoredItem item, Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        item.setUserEmail(user.getEmail());
        monitoredItemRepository.save(item);
        return "redirect:/monitor";
    }

    @GetMapping("/add")
    public String showAddItemForm(Model model) {
        model.addAttribute("monitoredItem", new MonitoredItem());
        return "add-item";
    }
}