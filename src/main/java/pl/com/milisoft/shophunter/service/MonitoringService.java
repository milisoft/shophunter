package pl.com.milisoft.shophunter.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.com.milisoft.shophunter.model.MonitoredItem;
import pl.com.milisoft.shophunter.repository.MonitoredItemRepository;

import java.io.IOException;
import java.util.List;

@Service
public class MonitoringService {

    @Autowired
    private MonitoredItemRepository repository;

    @Autowired
    private JavaMailSender mailSender;

    @Scheduled(fixedRate = 60000)
    public void checkForNewItems() {
        List<MonitoredItem> items = repository.findAll();
        for (MonitoredItem item : items) {
            if (isItemAvailable(item)) {
                sendNotification(item);
            }
        }
    }

    private boolean isItemAvailable(MonitoredItem item) {
        try {
            Document doc = Jsoup.connect(item.getSiteUrl()).get();
            // Przykład prostej analizy HTML. W rzeczywistości może być bardziej skomplikowane w zależności od struktury strony.
            return doc.text().contains(item.getItemName());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void sendNotification(MonitoredItem item) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(item.getUserEmail());
        message.setSubject("Item Available: " + item.getItemName());
        message.setText("The item you are monitoring is now available: " + item.getSiteUrl());
        mailSender.send(message);
    }
}