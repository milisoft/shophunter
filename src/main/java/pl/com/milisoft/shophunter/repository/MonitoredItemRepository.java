package pl.com.milisoft.shophunter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.milisoft.shophunter.model.MonitoredItem;

import java.util.List;

@Repository
public interface MonitoredItemRepository extends JpaRepository<MonitoredItem, Long> {
    List<MonitoredItem> findByUserEmail(String userEmail);
}