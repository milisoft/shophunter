package pl.com.milisoft.shophunter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShophunterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShophunterApplication.class, args);
	}

}
