CREATE TABLE IF NOT EXISTS user (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL UNIQUE,
    provider VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS monitored_item (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    item_name VARCHAR(255),
    country VARCHAR(255),
    site_url VARCHAR(255),
    user_email VARCHAR(255),
    CONSTRAINT fk_user_email FOREIGN KEY (user_email) REFERENCES user (email)
);